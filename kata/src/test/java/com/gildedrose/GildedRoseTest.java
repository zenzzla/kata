package com.gildedrose;

import static org.hamcrest.Matchers.in;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

import org.junit.Test;

public class GildedRoseTest {
	private static final String CONJURED = "Conjured";
	private static final String SULFURAS = "Sulfuras, Hand of Ragnaros";
	private static final String AGED_BRIE = "Aged Brie";
	private static final String BACKSTAGE_PASSES = "Backstage passes to a TAFKAL80ETC concert";

	// Statements for Sulfuras:
	// "Sulfuras" is a legendary item and as such its Quality is 80 and it never alters.
	@Test
	public void testSulfuras() {
		Item item = new Item(SULFURAS, 50, 80);

		GildedRose.updateQuality(item);

		assertEquals(SULFURAS, item.name);
		assertEquals("sellin never change after an updateQality", 50, item.sellIn);
		assertEquals("quality never change after an updateQality", 80, item.quality);
	}

	// Statements for Aged Brie:
	// - "Aged Brie" actually increases in Quality the older it gets
	// - The Quality of an item is never more than 50
	@Test
	public void testAgedBrie() {
		Item item = new Item(AGED_BRIE, 30, 30);
		GildedRose.updateQuality(item);
		// "Aged Brie" actually increases in Quality the older it gets
		assertEquals(AGED_BRIE, item.name);
		assertEquals("The sellIn should decrease by 1", 29, item.sellIn);
		assertEquals("The quality should increase by 1", 31, item.quality);

		
		item = new Item(AGED_BRIE, 0, 50);
		GildedRose.updateQuality(item);
		// The Quality of an item is never more than 50
		assertEquals(AGED_BRIE, item.name);
		assertEquals(-1, item.sellIn);
		assertEquals("Quality of Aged Brie can't be over 50", 50, item.quality);
	}
	
	// Statements for Backstage passes:
	// - "Backstage passes", like aged brie, increases in Quality as its SellIn value approaches;
	// - Quality increases by 2 when there are 10 days or less and by 3 when there are 5 days or less.
	// - Quality drops to 0 after the concert
	// - The Quality of an item is never more than 50
	// - The Quality of an item is never negative
	// - Quality of 'Backstage passes' should passe to 0 after the concert
	@Test
	public void testBackstagePasses() {
		Item item = new Item(BACKSTAGE_PASSES, 30, 30);
		GildedRose.updateQuality(item);

		assertEquals(BACKSTAGE_PASSES, item.name);
		// increases in Quality as its SellIn value approaches
		assertEquals("The sellIn should decrease by 1", 29, item.sellIn);
		assertEquals("The quality should increase by 1", 31, item.quality);

		
		item = new Item(BACKSTAGE_PASSES, 10, 30);
		GildedRose.updateQuality(item);
		// Quality increases by 2 when there are 10 days or less 
		assertEquals("Quality of 'Backstage passes' should be 32", 32, item.quality);
		
		
		item = new Item(BACKSTAGE_PASSES, 6, 30);
		// Quality increases by 2 when there are 10 days or less (and greater than 5)
		GildedRose.updateQuality(item);
		assertEquals("Quality of 'Backstage passes' should be 32", 32, item.quality);
		
		
		item = new Item(BACKSTAGE_PASSES, 5, 30);
		// Quality increases by 3 when there are 5 days or less
		GildedRose.updateQuality(item);
		assertEquals("Quality of 'Backstage passes' should be 33", 33, item.quality);
		
		
		item = new Item(BACKSTAGE_PASSES, 5, 50);
		// The Quality of an item is never more than 50
		GildedRose.updateQuality(item);
		assertEquals("Quality of 'Backstage passes' shouldn't be changed if it equals 50", 50, item.quality);
		
		item = new Item(BACKSTAGE_PASSES, 0, 50);
		// The Quality of an item is never more than 50
		GildedRose.updateQuality(item);
		assertEquals("The sellIn should decrease by 1", -1, item.sellIn);
		assertEquals("Quality of 'Backstage passes' should passe to 0 after the concert", 0, item.quality);
	}
	
	// Statements for ordinary product (other products than 'Sulfuras', 'Backstage passes' and 'Aged Brie')
	// - When the sell by hasn't passed yet, Quality degrades by one
	// - Once the sell by date has passed, Quality degrades twice as fast
	// - The Quality of an item is never negative
	@Test
	public void testOrdinaryProduct() {

		String oridinary = "Ordinary";
		// Precondition for ordinary item
		assertThat(oridinary, is(not(in(new String[] { SULFURAS, BACKSTAGE_PASSES, AGED_BRIE, BACKSTAGE_PASSES }))));
		
		Item item = new Item(oridinary, 30, 30);
		GildedRose.updateQuality(item);
		// When the sell by hasn't passed yet, Quality degrades by one
		assertEquals(oridinary, item.name);
		assertEquals("The sellIn should decrease by 1", 29, item.sellIn);
		assertEquals("The quality should increase by 1", 29, item.quality);

		
		item = new Item(oridinary, 0, 30);
		GildedRose.updateQuality(item);
		// Once the sell by date has passed, Quality degrades twice as fast
		assertEquals(oridinary, item.name);
		assertEquals("The sellIn should decrease by 1", -1, item.sellIn);
		assertEquals("The quality should decrease by 2", 28, item.quality);
		
		
		item = new Item(oridinary, 0, 0);
		GildedRose.updateQuality(item);
		// The Quality of an item is never negative
		assertEquals("Quality of other products must stay 0 when it's 0 before update", 0, item.quality);
	}
	
	// Statements for Conjured 
	// - "Conjured" items degrade in Quality twice as fast as normal items
	@Test
	public void testConjured() {
		Item item = new Item(CONJURED, 30, 30);
		GildedRose.updateQuality(item);

		assertEquals(CONJURED, item.name);
		// Decrease the qualit in Quality as its SellIn value approaches
		assertEquals("The sellIn should decrease by 1", 29, item.sellIn);
		assertEquals("The quality should decrease by 2", 28, item.quality);
		
		item = new Item(CONJURED, 0, 30);
		GildedRose.updateQuality(item);

		assertEquals(CONJURED, item.name);
		// increases in Quality as its SellIn value approaches
		assertEquals("The quality should decrease by 4", 26, item.quality);
	}
	
	@Test
	public void testUpdateQualityWihItmesOfGildedRose() {
		Item sulfurasItem = new Item(SULFURAS, 30, 80);
		Item oridinaryItem = new Item("Bad quality", 0, 20);
		
		GildedRose gildedRose = new GildedRose(new Item[] { sulfurasItem, oridinaryItem }); 
		
		// Preconditions for gildedRose
		assertNotNull(gildedRose.items);
		assertEquals(2, gildedRose.items.length);
		// Preconditions for sulfurasItem
		assertEquals(SULFURAS, sulfurasItem.name);
		assertEquals(30, sulfurasItem.sellIn);
		assertEquals(80, sulfurasItem.quality);
		// Preconditions for oridinaryItem
		assertEquals("Bad quality", oridinaryItem.name);
		assertEquals(0, oridinaryItem.sellIn);
		assertEquals(20, oridinaryItem.quality);
		
		gildedRose.updateQuality();
		// Check for sulfurasItem
		assertEquals(SULFURAS, sulfurasItem.name);
		assertEquals(30, sulfurasItem.sellIn);
		assertEquals(80, sulfurasItem.quality);
		// Check for oridinaryItem
		assertEquals("Bad quality", oridinaryItem.name);
		assertEquals(-1, oridinaryItem.sellIn);
		assertEquals(18, oridinaryItem.quality);
	}
}