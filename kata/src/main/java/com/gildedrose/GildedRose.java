package com.gildedrose;

class GildedRose {
	private static final String SULFURAS_ITEM = "Sulfuras, Hand of Ragnaros";
	public static final String BACKSTAGE_PASSES_ITEM = "Backstage passes to a TAFKAL80ETC concert";
	public static final String AGED_BRIE_ITEM = "Aged Brie";
	public static final String CONJURED_ITEM = "Conjured";

	Item[] items;

	public GildedRose(Item[] items) {
		this.items = items;
	}

	protected static void updateQuality(Item item) {
		String name = item.name;

		if (SULFURAS_ITEM.equals(name)) {
			// We do nothing
			return;
		}

		// Decreasing the number of the day
		--item.sellIn;

		int sellIn = item.sellIn;
		int quality = item.quality;

		if (AGED_BRIE_ITEM.equals(name)) {
			++quality;

		} else if (BACKSTAGE_PASSES_ITEM.equals(name)) {
			if (sellIn < 0) {
				quality = 0;

			} else if (sellIn < 5) {
				quality += 3;

			} else if (sellIn < 10) {
				quality += 2;

			} else {
				++quality;
			}

		} else {
			int factor = 1;
			
			if(CONJURED_ITEM.equals(name)) {
				factor = 2;
			}
			
			if (item.quality > 0) {
				if (sellIn < 0) {
					quality -= 2 * factor;

				} else {
					quality -= factor;
				}
			}
		}

		// The quality never over 50 for all items except for Sulfuras
		if (quality > 50) {
			quality = 50;
		}

		item.quality = quality;
	}

	public void updateQuality() {
		for (int i = 0; i < items.length; i++) {
			updateQuality(items[i]);
		}
	}
}