## Strategy of solving this problem ##

After reading the exercise and checking the existing code and before adding the new feature, the first thing I figure out is that the  code needs to be refactored in order to make it easy to add the new feature. But before refactoring the code we should make sure that we're not going to alter the behavior of the existing features and we're not going to introduce any regression to the **updateQuality** method, so in order to ensure that, we should write unit tests for all the existing features before making any changes to GildedRose class. All the unit tests should pass before adding the new feature. After that we add a new test for the new feature and after that adding the code the new feature.

## Summary of the steps to do ##
- Add unit tests for each existing feature (all of them should pass)
- Add a unit test for the new feature (the test should fail since the new feature hasn't already been added)
- Add the new feature (All the test should pass)

For each unit test and adding the new feature will be a separate commit. This will show the progress of the development more clear and what we did for each step.

## Testing ##
This is a simple gradle project, you can import it into any IDE supporting gradle and java, for my case, I've developed the solution using Eclipse.

All what you have to do is to execute tests of the class com.gildedrose.GildedRoseTest under src/test/java, this test class contains 6 tests. You can execute the tests under the IDE you're using or by issuing the following command under the root project folder :

`kata> gradle test -i`

Make sure there is no error in the output. You can check the result all test by opening the `kata/build/reports/tests/test/classes/com.gildedrose.GildedRoseTest.html` file with the browser